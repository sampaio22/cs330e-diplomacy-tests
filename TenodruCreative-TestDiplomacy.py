#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Authors: Alex Kong, Catherine Kim
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    # Write unit test here
    # We need at least 3

    def test_solve(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Berlin Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC Berlin\n")

    def test_solve_2(self):
        r = StringIO("A Boston Hold\nB Orlando Move Boston\nC Busan Support B\nD Florence Support B\nE Washington Support A\nF Sacramento Support A\nG Montreal Move Washington\nH Vancouver Move Sacramento\nI Kyoto Support B\nJ Ilsan Support A\nK Anchorage Move Boston\nL Denver Move Boston\nM NewOrleans Support H\nN Cancun Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Boston\nC Busan\nD Florence\nE [dead]\nF [dead]\nG [dead]\nH Sacramento\nI Kyoto\nJ Ilsan\nK [dead]\nL [dead]\nM NewOrleans\nN Cancun\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover

"""
