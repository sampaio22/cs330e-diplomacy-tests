#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, l, u = diplomacy_read(s)
        self.assertEqual(i,  'A')
        self.assertEqual(j, 'Madrid')
        self.assertEqual(l,  'Hold')
        self.assertEqual(u, 0)

    def test_read_2(self):
        s = "B Paris Support C\n"
        i, j, l, u = diplomacy_read(s)
        self.assertEqual(i,  'B')
        self.assertEqual(j, 'Paris')
        self.assertEqual(l,  'Support')
        self.assertEqual(u, 'C')

    def test_read_3(self):
        s = "D Austin Move Canada\n"
        i, j, l, u = diplomacy_read(s)
        self.assertEqual(i,  'D')
        self.assertEqual(j, 'Austin')
        self.assertEqual(l,  'Move')
        self.assertEqual(u, 'Canada')
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = dict(diplomacy_eval(['A Madrid Hold']))
        self.assertEqual(v, {'A': 'Madrid'})

    def test_eval_2(self):
        v = dict(diplomacy_eval(['A Madrid Hold', 'C Milan Support B', 'E Whatever Move Prague']))
        self.assertEqual(v, {'A': 'Madrid', 'C': 'Milan', 'E': 'Prague'})

    def test_eval_3(self):
        v = dict(diplomacy_eval(['A Madrid Move Siberia', 'Q Milan Move Finland', 'E Whatever Move Over']))
        self.assertEqual(v, {'A': 'Siberia', 'Q': 'Finland', 'E': 'Over'})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', 'Botswana')
        self.assertEqual(w.getvalue(), "A Botswana\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'D', 'Moscow')
        self.assertEqual(w.getvalue(), "D Moscow\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'C', '[Dead]')
        self.assertEqual(w.getvalue(), "C [Dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Paris Hold\nC Moscow Hold\nD Rome Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Paris\nC Moscow\nD Rome\n")

    def test_solve_2(self):
        r = StringIO("L London Move Moscow\nM Paris Move Moscow\nN Rome Move Moscow\nO Oslo Move Moscow\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "L [dead]\nM [dead]\nN [dead]\nO [dead]\n")

    def test_solve_3(self):
        r = StringIO("Q London Move Paris\nR Madrid Move Paris\nS Rome Support Q\nT London Support R\nU Oslo Support R\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "Q [dead]\nR Paris\nS Rome\nT London\nU Oslo\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
