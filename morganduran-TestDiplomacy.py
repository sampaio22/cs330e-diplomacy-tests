# TestDiplomacy.py 

# -------
# imports
# -------


from io import StringIO
from unittest import main, TestCase


from Diplomacy import Army, diplomacy_read, diplomacy_war, diplomacy_print, diplomacy_runner, diplomacy_solve


# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):
    
    # ---------
    # test read
    # ---------

    def test_read_1(self):
        s = ['A Madrid Hold']
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, 'A')
        self.assertEqual(j, 'Madrid')
        self.assertEqual(k, 'Hold')
    
    def test_read_2(self):
        s = ['A Barcelona Move Madrid']
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, 'A')
        self.assertEqual(j, 'Barcelona')
        self.assertEqual(k, 'Move')
        self.assertEqual(l, 'Madrid')
    
    def test_read_3(self):
        s = ['D Paris Support B']
        i, j, k ,l = diplomacy_read(s)
        self.assertEqual(i, 'D')
        self.assertEqual(j, 'Paris')
        self.assertEqual(k, 'Support')
        self.assertEqual(l, 'B')

    # ----------
    # test class
    # ----------

    def test_class_1(self):
        i, j, k = 'A', 'Madrid', 'Hold'
        army = Army(i, j, k)
        self.assertEqual(i, army.name)
        self.assertEqual(j, army.city)
        self.assertEqual(k, army.action)


    # -----------
    # test runner
    # -----------

    def test_runner_1(self):
        moves = [['A', 'Madrid', 'Hold']]
        x = diplomacy_runner(moves)
        self.assertEqual(x, ['A', 'Madrid'])

    def test_runner_2(self):
        moves = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
        x = diplomacy_runner(moves)
        self.assertEqual(x, ['A', '[dead]'], ['B', '[dead]'])

    def test_runner_3(self):
        moves = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        x = diplomacy_runner(moves)
        self.assertEqual(x, ['A', '[dead]'], ['B', 'Madrid'], ['C', 'London'])

    def test_runner_4(self):
        moves = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A'], ['F', 'Berlin', 'Support', 'A']]
        x = diplomacy_runner(moves)
        self.assertEqual(x, ['A', 'Madrid'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin'], ['F', 'Berlin'])


    # --------
    # test war
    # --------

    def test_war_1(self):
        w = Army('A', 'Madrid', 'Hold')
        x = {'Madrid' : w}
        y = [['A', w]]
        z = {}
        a = diplomacy_war(x, y, z)
        self.assertEqual(a, ['A Madrid'])

    # ----------
    # test print
    # ----------

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A', 'Madrid')
        self.assertEqual(w.getvalue(), 'A Madrid\n')

    def test_print_2(self):
        w =StringIO()
        diplomacy_print(w, 'B', '[dead]')
        self.assertEqual(w.getvalue(), 'B [dead]\n')

    def test_print_3(self):
        w = StringIO
        diplomacy_print(w, 'E', 'Austin')
        self.assertEqual(w.getvalue(), 'E Austin\n')


    # ----------
    # test solve
    # ----------

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")


    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_solve_4(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Berlin Support A')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB [dead]\nC [dead]\nD Paris\nE Austin\nF Berlin')

