from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, \
    diplomacy_solve


class TestDiplomacy (TestCase):

    def test_eval1(self):
        inp = ['A Madrid Hold']
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)

        CorrectResult = 'A Madrid\n'
        self.assertEqual(result, CorrectResult)

    def test_eval2(self):
        inp = [
                'A Madrid Hold',
                'B Barcelona Move Madrid',
                'C London Support B'
                ]
        CorrectResult = 'A [dead]\nB Madrid\nC London\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)

        self.assertEqual(result, CorrectResult)

    def test_eval3(self):
        inp = ['A Madrid Hold', 'B Barcelona Move Madrid']
        CorrectResult = 'A [dead]\nB [dead]\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)

        self.assertEqual(result, CorrectResult)

    def test_eval4(self):
        inp = [
                'A Madrid Hold',
                'B Barcelona Move Madrid',
                'C London Support B',
                'D Austin Move London'
                ]
        CorrectResult = 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)
        self.assertEqual(result, CorrectResult)

    def test_eval5(self):
        inp = [
                'A Madrid Hold',
                'B Barcelona Move Madrid',
                'C London Move Madrid',
                ]
        CorrectResult = 'A [dead]\nB [dead]\nC [dead]\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)
        self.assertEqual(result, CorrectResult)

    def test_eval6(self):
        inp = [
                'A Madrid Hold',
                'B Barcelona Move Madrid',
                'C London Move Madrid',
                'D Paris Support B'
                ]
        CorrectResult = 'A [dead]\nB Madrid\nC [dead]\nD Paris\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)
        self.assertEqual(result, CorrectResult)

    def test_eval7(self):
        inp = [
                'A Madrid Hold',
                'B Barcelona Move Madrid',
                'C London Move Madrid',
                'D Paris Support B',
                'E Austin Support A'
                ]
        CorrectResult = 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n'
        s = diplomacy_read(inp)
        result = diplomacy_eval(s)
        self.assertEqual(result, CorrectResult)


if __name__ == "__main__":
    main()
