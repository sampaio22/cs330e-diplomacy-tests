# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import *

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a,  ["A", "Madrid", "support", "B"])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        l = ["A", "London"]
        diplomacy_print(w, l)
        self.assertEqual(w.getvalue(), "A London\n")

    # -----
    # board
    # -----
    def test_board1(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "Paris", "hold"]]
        b = {"Madrid": [1, 0], "Paris": [0, 1]}
        self.assertEqual(b, make_board(moves))

    def test_board2(self):
        moves = [["A", "Madrid", "support", "B"], [
            "B", "Paris", "hold"], ["C", "Paris", "hold"]]
        b = {"Madrid": [1, 0, 0], "Paris": [0, 1, 1]}
        self.assertEqual(b, make_board(moves))

    # -----
    # move
    # -----
    def test_move1(self):
        moves = [["A", "Madrid", "move", "London"]]
        pre = {"Madrid": [1]}
        post = {"Madrid": [0], "London": [1]}
        self.assertEqual(post, make_move(moves, pre)[1])

    def test_move2(self):
        moves = [["A", "Madrid", "move", "London"], ["B", "London", "hold"]]
        pre = {'Madrid': [1, 0], 'London': [0, 1]}
        post = {"Madrid": [0, 0], "London": [1, 1]}
        self.assertEqual(post, make_move(moves, pre)[1])

    def test_move3(self):
        moves = [["A", "Madrid", "move", "Colombia"], ["B", "London", "hold"]]
        pre = {'Madrid': [1, 0], 'London': [0, 1]}
        post = {"Madrid": [0, 0], "London": [0, 1], "Colombia": [1, 0]}
        self.assertEqual(post, make_move(moves, pre)[1])
    def test_move4(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"], ["C", "Dallas", "move", "Madrid"], ["D","Kansas","move", "London"],["E", "Brazil","support","A"],["F","Paris","Support","D"]]
        pre = {'Madrid': [1, 0,0,0,0,0], 'London': [0, 1,0,0,0,0], 'Dallas':[0, 0,1,0,0,0], 'Kansas':[0, 0,0,1,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        post = {'Madrid': [1, 0,1,0,0,0], 'London': [0, 1,0,1,0,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        self.assertEqual(post, make_move(moves, pre)[1])


    # -----
    # check
    # -----

    def test_check1(self):
        moves = [["A", "Madrid", "move", "London"], ["B", "London", "hold"]]
        board = {'Madrid': [0, 0], 'London': [1, 1]}
        post = {"Madrid": [0, 0], "London": [0, 0]}
        self.assertEqual(post, check(moves, board))

    def test_check2(self):
        moves = [["A", "Madrid", "hold"], ["B", "London", "hold"]]
        board = {'Madrid': [1, 0], 'London': [0, 1]}
        post = {"Madrid": [1, 0], "London": [0, 1]}
        self.assertEqual(post, check(moves, board))

    def test_check3(self):
        moves = [["A", "Madrid", "support", "B"], [
            "B", "Paris", "hold"], ["C", "Paris", "hold"]]
        board = {'Madrid': [1, 0, 0], 'Paris': [0, 1, 1]}
        post = {"Madrid": [1, 0, 0], "Paris": [0, 0, 0]}
        self.assertEqual(post, check(moves, board))
    def test_check4(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"], ["C", "Dallas", "move", "Madrid"], ["D","London","move", "Londond"],["E", "Brazil","support","A"],["F","Paris","Support","D"]]
        board = post = {'Madrid': [1, 0,0,0,0,0], 'London': [0, 1,0,1,0,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        post = {'Madrid': [1, 0,0,0,0,0], 'London': [0, 0,0,0,0,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        self.assertEqual(post, check(moves, board))

    # -----
    # support
    # -----

    def test_support1(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"]]
        board = {'Madrid': [1, 0], 'London': [0, 1]}
        post = {"Madrid": [1, 0], "London": [0, 2]}
        self.assertEqual(post, support(moves, board))

    def test_support2(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"]]
        board = {'Madrid': [0, 0], 'London': [0, 1]}
        post = {"Madrid": [0, 0], "London": [0, 1]}
        self.assertEqual(post, support(moves, board))

    def test_support3(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"]]
        board = {'Madrid': [1, 0], 'London': [0, 0]}
        post = {"Madrid": [1, 0], "London": [0, 1]}
        self.assertEqual(post, support(moves, board))
    def test_support4(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"], ["C", "Dallas", "move", "Madrid"], ["D","London","move", "Londond"],["E", "Brazil","support","A"],["F","Paris","Support","D"]]
        board = {'Madrid': [0, 0,0,0,0,0], 'London': [0, 0,0,0,0,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        post = {'Madrid': [1, 0,0,0,0,0], 'London': [0, 1,0,1,0,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        self.assertEqual(post, support(moves, board))

    # -----
    # final
    # -----

    def test_final1(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"]]
        board = {'Madrid': [1, 0], 'London': [0, 2]}
        post = [["A", "Madrid"], ["B", "London"]]
        self.assertEqual(post, final(moves, board))

    def test_final2(self):
        moves = [["A", "Madrid", "Move", "London"], ["B", "London", "hold"]]
        board = {'Madrid': [0, 0], 'London': [1, 1]}
        post = [["A", "[dead]"], ["B", "[dead]"]]
        self.assertEqual(post, final(moves, board))

    def test_final3(self):
        moves = [["A", "Madrid", "support", "B"], [
            "B", "London", "hold"], ["C", "London", "hold"]]
        board = {'Madrid': [1, 0, 0], 'London': [0, 2, 1]}
        post = [["A", "Madrid"], ["B", "London"], ["C", "[dead]"]]
        self.assertEqual(post, final(moves, board))
    def test_final4(self):
        moves = [["A", "Madrid", "support", "B"], ["B", "London", "hold"], ["C", "Dallas", "move", "Madrid"], ["D","London","move", "Londond"],["E", "Brazil","support","A"],["F","Paris","Support","D"]]
        board = {'Madrid': [1, 0,0,0,0,0], 'London': [0, 0,1,0,1,0], 'Dallas':[0, 0,0,0,0,0], 'Kansas':[0, 0,0,0,0,0],'Brazil':[0, 0,0,0,1,0], 'Paris':[0, 0,0,0,0,1]}
        post = [["A","Madrid"],["B","[dead]"],["C","[dead]"],["D","[dead]"],["E","Brazil"], ["F","Paris"]]
        self.assertEqual(post, final(moves, board))

    # ----
    # diplomacy_solve
    # ----
    def test_dip_solve1(self):
        w = StringIO()
        r = StringIO("A Madrid hold\n B London hold")
        b = StringIO("A Madrid\nB London\n")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), b.getvalue())
    def test_dip_solve2(self):
        w = StringIO()
        r = StringIO("A Madrid hold\n B London move Madrid")
        b = StringIO("A [dead]\nB [dead]\n")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), b.getvalue())
    def test_dip_solve3(self):
        w = StringIO()
        r = StringIO("A Madrid move Paris\n B London hold")
        b = StringIO("A Paris\nB London\n")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), b.getvalue())

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
